import React, { Component, createRef } from "react";
import "./Modal.scss";
class Modal extends Component {
  constructor(props) {
    super(props);
    this.myRef = createRef();
  }
  outsideCloseHandler = (e) => {
    if (e.target.contains(this.myRef.current)) {
      this.props.onClose();
    }
  };
  render() {
    const { header, closeButton, onClose, text, actions } = this.props;
    return (
      <div className="modal__wrapper" onClick={this.outsideCloseHandler}>
        <div ref={this.myRef} className={"modal"}>
          <div className="modal__header">
            <span className="modal__header__title">{header}</span>
            {closeButton && (
              <span className="modal__header__close__btn" onClick={onClose}>
                X
              </span>
            )}
          </div>
          <p className="modal__message">{text}</p>
          {actions}
        </div>
      </div>
    );
  }
}
export default Modal;
