import React, { Component } from "react";
import "./Button.scss";

class Button extends Component {
  render() {
    const { text, onPress, backgroundColor } = this.props;
    return (
      <button
        style={{ background: backgroundColor }}
        className={"btn"}
        onClick={onPress}
        type="button"
      >
        {text}
      </button>
    );
  }
}
export default Button;
