import React, { Component } from "react";
import "./App.scss";
import Modal from "./components/modals/Modal";
import Button from "./components/buttons/Button";
class App extends Component {
  state = {
    modal: null,
  };
  actionModalHandler = (status) => {
    this.setState({
      modal: status,
      //modal: "FIRST" || modal: "SECOND"
    });
  };
  render() {
    const { modal } = this.state;
    const modals = {
      FIRST: (
        <Modal
          header={"Do you want to delete this file ?"}
          text={
            <span>
              Once you delete this file, it won't be possible to undo this
              action. <br />
              Are you sure you want to delete it?
            </span>
          }
          closeButton={true}
          actions={
            <div className="modal__btn__container">
              <button className="modal__btn agree__btn">Ok</button>
              <button
                className="modal__btn cancel__btn"
                onClick={() => {
                  this.actionModalHandler(null);
                }}
              >
                Cancel
              </button>
            </div>
          }
          onClose={() => {
            this.actionModalHandler(null);
          }}
        />
      ),
      SECOND: (
        <Modal
          header={
            "Do you want us to call you later to clarify all the details ?"
          }
          text={
            <span>
              Please, be ready with the questions to our specialist when he
              calls you. <br />
              Do you agree on the following phone session in 5 minutes? Are you
              free to talk?
            </span>
          }
          closeButton={true}
          actions={
            <div className="modal__btn__container">
              <button className="modal__btn agree__btn">Yes, sure</button>
              <button
                className="modal__btn cancel__btn"
                onClick={() => {
                  this.actionModalHandler(null);
                }}
              >
                No, I'm busy
              </button>
            </div>
          }
          onClose={() => {
            this.actionModalHandler(null);
          }}
        />
      ),
    };
    const activeModal = modals[modal] ?? null;
    return (
      <div className={"app__container"}>
        {activeModal}
        <div className="btn__container">
          <Button
            text={"Open first modal"}
            onPress={() => {
              this.actionModalHandler("FIRST");
            }}
            backgroundColor={"midnightblue"}
          />
          <Button
            text={"Open second modal"}
            backgroundColor={"purple"}
            onPress={() => {
              this.actionModalHandler("SECOND");
            }}
          />
        </div>
      </div>
    );
  }
}
export default App;
